/*
 Navicat Premium Data Transfer

 Source Server         : Mysql57
 Source Server Type    : MySQL
 Source Server Version : 50737
 Source Host           : localhost:3306
 Source Schema         : dbdesign

 Target Server Type    : MySQL
 Target Server Version : 50737
 File Encoding         : 65001

 Date: 13/02/2023 11:00:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order`  (
  `oid` int(10) NOT NULL AUTO_INCREMENT COMMENT '订单号',
  `pid` int(5) NULL DEFAULT NULL COMMENT '顾客卡号',
  `rid` int(4) NULL DEFAULT NULL COMMENT '房间号',
  `start` datetime(0) NULL DEFAULT NULL COMMENT '入住时间',
  `end` datetime(0) NULL DEFAULT NULL COMMENT '退房时间',
  `total` decimal(10, 2) NULL DEFAULT NULL COMMENT '总费用',
  `flag` int(1) NULL DEFAULT 0 COMMENT '是否结账0未结账1已结账',
  PRIMARY KEY (`oid`) USING BTREE,
  INDEX `order___fk1`(`pid`) USING BTREE,
  INDEX `order___fk2`(`rid`) USING BTREE,
  CONSTRAINT `order___fk1` FOREIGN KEY (`pid`) REFERENCES `person` (`pid`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `order___fk2` FOREIGN KEY (`rid`) REFERENCES `room` (`rid`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '订单' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES (1, 2, 102, '2022-07-16 10:51:48', '2022-07-23 09:08:12', 720.00, 1);
INSERT INTO `order` VALUES (3, 15, 302, '2022-07-23 09:02:48', '2022-08-24 12:11:45', 3840.00, 1);
INSERT INTO `order` VALUES (4, 5, 501, '2022-07-23 09:02:53', '2022-07-26 18:07:12', 1500.00, 1);
INSERT INTO `order` VALUES (5, 7, 507, '2022-07-23 09:03:04', '2022-07-23 09:03:04', 0.00, 0);
INSERT INTO `order` VALUES (6, 14, 109, '2022-07-23 09:03:26', '2022-07-26 18:07:14', 600.00, 1);
INSERT INTO `order` VALUES (7, 9, 104, '2022-07-23 09:03:56', '2022-08-04 14:13:54', 960.00, 1);
INSERT INTO `order` VALUES (8, 11, 105, '2022-07-23 09:04:05', '2022-08-04 14:13:49', 1440.00, 1);
INSERT INTO `order` VALUES (9, 12, 303, '2022-07-23 09:04:10', '2022-08-04 14:13:48', 1440.00, 1);
INSERT INTO `order` VALUES (10, 13, NULL, '2022-07-23 09:04:37', '2022-07-26 18:07:15', 240.00, 1);
INSERT INTO `order` VALUES (12, 8, 305, '2022-07-23 09:05:23', '2022-07-26 18:07:17', 360.00, 1);
INSERT INTO `order` VALUES (13, 9, 310, '2022-07-23 09:05:30', '2022-08-04 14:13:49', 1440.00, 1);
INSERT INTO `order` VALUES (14, 5, 210, '2022-07-23 09:05:53', '2022-07-23 09:05:53', 0.00, 0);
INSERT INTO `order` VALUES (15, 25, 107, '2022-07-23 09:07:03', '2022-07-26 18:07:18', 240.00, 1);
INSERT INTO `order` VALUES (16, 26, 108, '2022-07-23 09:07:11', '2022-07-23 09:07:11', 0.00, 0);
INSERT INTO `order` VALUES (17, 30, 106, '2022-07-23 09:07:16', '2022-08-04 14:13:51', 2400.00, 1);
INSERT INTO `order` VALUES (18, 11, 204, '2022-07-23 09:09:36', '2022-08-04 14:13:50', 960.00, 1);
INSERT INTO `order` VALUES (19, 12, 205, '2022-07-23 09:09:50', '2022-07-23 09:09:50', 0.00, 0);
INSERT INTO `order` VALUES (20, 14, 206, '2022-07-23 09:09:56', '2022-07-23 09:09:56', 0.00, 0);
INSERT INTO `order` VALUES (21, 17, 207, '2022-07-23 09:10:04', '2022-07-23 09:10:04', 0.00, 0);
INSERT INTO `order` VALUES (22, 19, 304, '2022-07-23 09:10:18', '2022-08-04 14:13:52', 1440.00, 1);
INSERT INTO `order` VALUES (23, 33, 401, '2022-07-23 09:10:51', '2022-07-23 09:10:51', 0.00, 0);
INSERT INTO `order` VALUES (24, 34, 403, '2022-07-23 09:10:57', '2022-07-23 09:10:57', 0.00, 0);
INSERT INTO `order` VALUES (25, 35, 404, '2022-07-23 09:11:03', '2022-07-23 09:11:03', 0.00, 0);
INSERT INTO `order` VALUES (26, 37, 502, '2022-07-23 09:11:10', '2022-07-23 09:11:10', 0.00, 0);
INSERT INTO `order` VALUES (27, 5, 102, '2022-08-04 14:13:22', '2022-08-04 14:13:22', 0.00, 0);
INSERT INTO `order` VALUES (28, 9, 508, '2022-08-04 14:13:43', '2022-08-04 14:13:43', 0.00, 0);
INSERT INTO `order` VALUES (29, 1, 501, '2022-08-04 15:02:41', '2022-08-04 15:02:41', 0.00, 0);
INSERT INTO `order` VALUES (30, 2, 303, '2022-08-04 15:02:47', '2022-08-04 15:02:47', 0.00, 0);
INSERT INTO `order` VALUES (31, 4, 101, '2022-08-04 15:02:59', '2022-08-04 15:02:59', 0.00, 0);
INSERT INTO `order` VALUES (32, 7, 306, '2022-08-04 15:03:07', '2022-08-04 15:03:07', 0.00, 0);
INSERT INTO `order` VALUES (33, 9, 307, '2022-08-04 15:03:24', '2022-08-04 15:03:24', 0.00, 0);
INSERT INTO `order` VALUES (34, 73, 109, '2022-08-04 20:26:34', '2022-08-04 20:26:34', 0.00, 0);
INSERT INTO `order` VALUES (35, 19, 201, '2022-08-06 13:36:14', '2022-08-06 13:36:14', 0.00, 0);
INSERT INTO `order` VALUES (36, 52, 203, '2022-08-15 14:04:13', '2022-08-15 14:04:13', 0.00, 0);
INSERT INTO `order` VALUES (37, 9, 103, '2022-08-15 14:04:32', '2022-08-15 14:04:32', 0.00, 0);

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `pid` int(5) NOT NULL AUTO_INCREMENT COMMENT '顾客编号',
  `pname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `psex` int(1) NULL DEFAULT NULL COMMENT '性别',
  `ppid` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '身份证号',
  `ptel` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  PRIMARY KEY (`pid`) USING BTREE,
  UNIQUE INDEX `person_ppid_uindex`(`ppid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 77 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '顾客' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES (1, 'dyz', 1, '111111111111111111', '11111111112');
INSERT INTO `person` VALUES (2, 'bby', 1, '222222222222222222', '22222222222');
INSERT INTO `person` VALUES (3, 'zqj', 1, '333333333333333333', '33333333333');
INSERT INTO `person` VALUES (4, 'lb', 1, '444444444444444444', '44444444444');
INSERT INTO `person` VALUES (5, 'ctx', 1, '555555555555555555', '55555555555');
INSERT INTO `person` VALUES (6, 'zwj', 1, '666666666666666666', '66666666666');
INSERT INTO `person` VALUES (7, 'zm', 0, '777777777777777777', '77777777777');
INSERT INTO `person` VALUES (8, 'cwj', 0, '140105199003077257', '18474006290');
INSERT INTO `person` VALUES (9, 'y', 0, '140105199003079973', '15629057701');
INSERT INTO `person` VALUES (10, 'sq', 1, '140105199003071058', '16507867255');
INSERT INTO `person` VALUES (11, 'dq', 0, '140105199003076836', '13842668793');
INSERT INTO `person` VALUES (12, 'lbds', 1, '14010519900307411X', '15909343794');
INSERT INTO `person` VALUES (13, 'ggz', 1, '140105199003070717', '14543638372');
INSERT INTO `person` VALUES (14, 'ds', 1, '140105199003071939', '18771596863');
INSERT INTO `person` VALUES (15, 'msy', 1, '140105199003076019', '14912671764');
INSERT INTO `person` VALUES (16, 'mq', 1, '140105199003079690', '18983221125');
INSERT INTO `person` VALUES (17, 'sb', 1, '140105199003075112', '13571386268');
INSERT INTO `person` VALUES (18, 'tyzr', 1, '120101199003073716', '13079917955');
INSERT INTO `person` VALUES (19, 'gy', 0, '120101199003073717', '15980962061');
INSERT INTO `person` VALUES (20, 'al', 0, '120101199003073718', '13027232482');
INSERT INTO `person` VALUES (21, 'my', 1, '120101199003073719', '13505309397');
INSERT INTO `person` VALUES (22, 'jl', 0, '120101199003073720', '17178387322');
INSERT INTO `person` VALUES (23, 'gsl', 0, '130202199003074974', '13989453664');
INSERT INTO `person` VALUES (24, 'blsy', 1, '130202199003074975', '19959056322');
INSERT INTO `person` VALUES (25, 'hy', 1, '130202199003074976', '19969916766');
INSERT INTO `person` VALUES (26, 'hz', 1, '130202199003074978', '18496207042');
INSERT INTO `person` VALUES (27, 'mkbl', 1, '130202199003074979', '18796354504');
INSERT INTO `person` VALUES (28, 'cjsh', 1, '500101199003077866', '17162322914');
INSERT INTO `person` VALUES (29, 'yj', 0, '500101199003077830', '13758497690');
INSERT INTO `person` VALUES (30, 'lyf', 1, '500101199003075712', '17854374990');
INSERT INTO `person` VALUES (31, 'drj', 1, '500101199003074293', '13123294422');
INSERT INTO `person` VALUES (32, 'lbqh', 1, '500101199003076256', '17238084406');
INSERT INTO `person` VALUES (33, 'ssx', 0, '500101199003075050', '13548238672');
INSERT INTO `person` VALUES (34, 'jc', 1, '310101199003076912', '18084724773');
INSERT INTO `person` VALUES (35, 'skz', 1, '310101199003071679', '13386762524');
INSERT INTO `person` VALUES (36, 'zk', 1, '310101199003076736', '13083576954');
INSERT INTO `person` VALUES (37, 'mz', 1, '310101199003070617', '17535187163');
INSERT INTO `person` VALUES (38, 'xs', 0, '310101199003072815', '17197033340');
INSERT INTO `person` VALUES (39, 'ce', 0, '220102199003071877', '17361148459');
INSERT INTO `person` VALUES (40, 'sgwe', 0, '220102199003071864', '17817625527');
INSERT INTO `person` VALUES (41, 'smx', 1, '220102199003071892', '18098717115');
INSERT INTO `person` VALUES (42, 'bzhw', 0, '220102199003071879', '17745694968');
INSERT INTO `person` VALUES (43, 'smy', 0, '220102199003071878', '13191057581');
INSERT INTO `person` VALUES (44, 'mld', 0, '220102199003071876', '17323724313');
INSERT INTO `person` VALUES (45, 'yyh', 0, '360102199003074855', '18777045572');
INSERT INTO `person` VALUES (46, 'yx', 0, '360102199003074878', '18189528523');
INSERT INTO `person` VALUES (47, 'nw', 0, '360102199003074873', '17171934670');
INSERT INTO `person` VALUES (48, 'zy', 0, '360102199003074876', '13271945044');
INSERT INTO `person` VALUES (49, 'my', 0, '360102199003074875', '18634673224');
INSERT INTO `person` VALUES (50, 'gj', 1, '410102199003079085', '13541941697');
INSERT INTO `person` VALUES (51, 'my', 1, '410102199003079062', '18356793532');
INSERT INTO `person` VALUES (52, 'zgl', 1, '410102199003079074', '13954727560');
INSERT INTO `person` VALUES (53, 'dc', 0, '410102199003079022', '18548543741');
INSERT INTO `person` VALUES (54, 'zl', 1, '410102199003079013', '17619797461');
INSERT INTO `person` VALUES (55, 'aql', 0, '410102199003077391', '18541453247');
INSERT INTO `person` VALUES (56, 'jzy', 0, '410102199003077370', '13224246162');
INSERT INTO `person` VALUES (57, 'wzt', 1, '410102199003077369', '13386929335');
INSERT INTO `person` VALUES (58, 'wzj', 0, '410102199003077338', '18393296717');
INSERT INTO `person` VALUES (59, 'f', 0, '410102199003077357', '15229895768');
INSERT INTO `person` VALUES (60, 'l', 1, '410102199003077332', '17758723834');
INSERT INTO `person` VALUES (61, 'j', 1, '410102199003077333', '15861495279');
INSERT INTO `person` VALUES (62, 'yzj', 1, '410102199003070996', '15355828491');
INSERT INTO `person` VALUES (63, 'yg', 1, '410102199003070994', '18213199098');
INSERT INTO `person` VALUES (64, 'blxc', 0, '410102199003070997', '18001492248');
INSERT INTO `person` VALUES (65, 'hml', 0, '410102199003070992', '17111598665');
INSERT INTO `person` VALUES (66, 'ak', 0, '410102199003070991', '15230598463');
INSERT INTO `person` VALUES (67, 'lb', 1, '410102199003071478', '17663183703');
INSERT INTO `person` VALUES (68, 'nkll', 1, '410102199003071473', '18377739398');
INSERT INTO `person` VALUES (69, 'llw', 1, '410102199003071475', '16559713838');
INSERT INTO `person` VALUES (70, 'ln', 1, '410102199003071474', '18371223361');
INSERT INTO `person` VALUES (71, 'hx', 1, '410102199003071479', '17364956770');
INSERT INTO `person` VALUES (72, 'gbwz', 0, '410102199003071476', '18667384472');
INSERT INTO `person` VALUES (73, 'ajmd', 1, '477777777777777777', '17825698543');
INSERT INTO `person` VALUES (74, 'ss', 1, '988999999999999999', '15682558866');
INSERT INTO `person` VALUES (76, 'sada', 1, '688115555555555555', '13151511112');

-- ----------------------------
-- Table structure for room
-- ----------------------------
DROP TABLE IF EXISTS `room`;
CREATE TABLE `room`  (
  `rid` int(4) NOT NULL DEFAULT 0 COMMENT '房间号',
  `tid` int(2) NULL DEFAULT NULL COMMENT '类型号',
  `sid` int(2) NULL DEFAULT 2 COMMENT '状态号',
  PRIMARY KEY (`rid`) USING BTREE,
  INDEX `room___fk_state`(`sid`) USING BTREE,
  INDEX `room___fk_type`(`tid`) USING BTREE,
  CONSTRAINT `room___fk_state` FOREIGN KEY (`sid`) REFERENCES `state` (`sid`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `room___fk_type` FOREIGN KEY (`tid`) REFERENCES `type` (`tid`) ON DELETE SET NULL ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '房间' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of room
-- ----------------------------
INSERT INTO `room` VALUES (101, 1, 1);
INSERT INTO `room` VALUES (102, 2, 1);
INSERT INTO `room` VALUES (103, 3, 1);
INSERT INTO `room` VALUES (104, 1, 2);
INSERT INTO `room` VALUES (105, 2, 2);
INSERT INTO `room` VALUES (106, 3, 2);
INSERT INTO `room` VALUES (107, 1, 2);
INSERT INTO `room` VALUES (108, 2, 1);
INSERT INTO `room` VALUES (109, 3, 1);
INSERT INTO `room` VALUES (201, 1, 1);
INSERT INTO `room` VALUES (203, 1, 1);
INSERT INTO `room` VALUES (204, 1, 2);
INSERT INTO `room` VALUES (205, 1, 1);
INSERT INTO `room` VALUES (206, 1, 1);
INSERT INTO `room` VALUES (207, 1, 1);
INSERT INTO `room` VALUES (208, 1, 2);
INSERT INTO `room` VALUES (209, 1, 2);
INSERT INTO `room` VALUES (210, 1, 1);
INSERT INTO `room` VALUES (301, 2, 2);
INSERT INTO `room` VALUES (302, 2, 2);
INSERT INTO `room` VALUES (303, 2, 1);
INSERT INTO `room` VALUES (304, 2, 2);
INSERT INTO `room` VALUES (305, 2, 2);
INSERT INTO `room` VALUES (306, 2, 1);
INSERT INTO `room` VALUES (307, 2, 1);
INSERT INTO `room` VALUES (308, 2, 2);
INSERT INTO `room` VALUES (309, 2, 2);
INSERT INTO `room` VALUES (310, 2, 2);
INSERT INTO `room` VALUES (401, 3, 1);
INSERT INTO `room` VALUES (402, 3, 2);
INSERT INTO `room` VALUES (403, 3, 1);
INSERT INTO `room` VALUES (404, 3, 1);
INSERT INTO `room` VALUES (405, 3, 2);
INSERT INTO `room` VALUES (406, 3, 2);
INSERT INTO `room` VALUES (407, 3, 2);
INSERT INTO `room` VALUES (408, 3, 2);
INSERT INTO `room` VALUES (409, 3, 2);
INSERT INTO `room` VALUES (410, 3, 2);
INSERT INTO `room` VALUES (501, 4, 1);
INSERT INTO `room` VALUES (502, 4, 1);
INSERT INTO `room` VALUES (503, 4, 2);
INSERT INTO `room` VALUES (504, 4, 2);
INSERT INTO `room` VALUES (505, 4, 2);
INSERT INTO `room` VALUES (506, 4, 2);
INSERT INTO `room` VALUES (507, 4, 1);
INSERT INTO `room` VALUES (508, 2, 1);
INSERT INTO `room` VALUES (509, 1, 2);
INSERT INTO `room` VALUES (510, 3, 2);
INSERT INTO `room` VALUES (601, 2, 2);
INSERT INTO `room` VALUES (602, 2, 2);

-- ----------------------------
-- Table structure for state
-- ----------------------------
DROP TABLE IF EXISTS `state`;
CREATE TABLE `state`  (
  `sid` int(2) NOT NULL DEFAULT 0 COMMENT '状态号',
  `sname` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态名',
  PRIMARY KEY (`sid`) USING BTREE,
  UNIQUE INDEX `state_sname_uindex`(`sname`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '状态' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of state
-- ----------------------------
INSERT INTO `state` VALUES (1, '已入住');
INSERT INTO `state` VALUES (2, '未入住');

-- ----------------------------
-- Table structure for type
-- ----------------------------
DROP TABLE IF EXISTS `type`;
CREATE TABLE `type`  (
  `tid` int(2) NOT NULL AUTO_INCREMENT COMMENT '类型号',
  `tname` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型名',
  `tprice` decimal(8, 2) NULL DEFAULT NULL COMMENT '价格',
  PRIMARY KEY (`tid`) USING BTREE,
  UNIQUE INDEX `type_tname_uindex`(`tname`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '房间类型' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of type
-- ----------------------------
INSERT INTO `type` VALUES (1, '单人房', 80.00);
INSERT INTO `type` VALUES (2, '标准房', 120.00);
INSERT INTO `type` VALUES (3, '豪华房', 200.00);
INSERT INTO `type` VALUES (4, '总统房', 500.00);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(3) NOT NULL AUTO_INCREMENT COMMENT 'ID号',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_username_uindex`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin');
INSERT INTO `user` VALUES (2, '消失的蓝', '123456');
INSERT INTO `user` VALUES (3, 'root', 'root');
INSERT INTO `user` VALUES (5, '202024100741', 'ssssss');

-- ----------------------------
-- View structure for orts
-- ----------------------------
DROP VIEW IF EXISTS `orts`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `orts` AS (select `o`.`oid` AS `oid`,`o`.`start` AS `start`,`o`.`end` AS `end`,`o`.`flag` AS `flag`,`o`.`total` AS `total`,`p`.`pid` AS `pid`,`p`.`pname` AS `pname`,`p`.`ptel` AS `ptel`,`p`.`psex` AS `psex`,`r`.`rid` AS `rid`,`s`.`sid` AS `sid`,`s`.`sname` AS `sname`,`t`.`tid` AS `tid`,`t`.`tname` AS `tname`,`t`.`tprice` AS `tprice` from ((((`order` `o` join `person` `p` on((`o`.`pid` = `p`.`pid`))) join `room` `r` on((`o`.`rid` = `r`.`rid`))) join `type` `t` on((`r`.`tid` = `t`.`tid`))) join `state` `s` on((`r`.`sid` = `s`.`sid`))));

-- ----------------------------
-- View structure for rts
-- ----------------------------
DROP VIEW IF EXISTS `rts`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `rts` AS (select `r`.`rid` AS `rid`,`r`.`tid` AS `tid`,`r`.`sid` AS `sid`,`t`.`tname` AS `tname`,`t`.`tprice` AS `price`,`s`.`sname` AS `sname` from ((`room` `r` join `type` `t` on((`r`.`tid` = `t`.`tid`))) join `state` `s` on((`r`.`sid` = `s`.`sid`))));

-- ----------------------------
-- Triggers structure for table order
-- ----------------------------
DROP TRIGGER IF EXISTS `insert-order`;
delimiter ;;
CREATE TRIGGER `insert-order` AFTER INSERT ON `order` FOR EACH ROW update room set sid = 1 where rid = new.rid
;
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table order
-- ----------------------------
DROP TRIGGER IF EXISTS `update-order`;
delimiter ;;
CREATE TRIGGER `update-order` AFTER UPDATE ON `order` FOR EACH ROW update room set sid = 2 where rid = new.rid
;
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table order
-- ----------------------------
DROP TRIGGER IF EXISTS `delete-order`;
delimiter ;;
CREATE TRIGGER `delete-order` AFTER DELETE ON `order` FOR EACH ROW update room set sid = 2 where rid = old.rid
;
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
