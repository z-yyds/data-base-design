package com.zwj.controller;


import com.zwj.Service.UserServiceImpl;
import com.zwj.mapper.UserMapper;
import com.zwj.pojo.User;

import com.zwj.utils.VerifyCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;


/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/22 21:41
 */
@Controller
public class UserController {

    @Autowired
    private UserServiceImpl userService;


    private User user;

    private String usercode;

    @Autowired
    JavaMailSenderImpl mailSender;

    public void code() {

        usercode = VerifyCodeUtil.getCode();

        SimpleMailMessage message = new SimpleMailMessage();
        message.setSubject("管理员授权码提醒");
        message.setText("有人正在注册成为工作人员，授权码为：" + usercode + "，若非本人操作,请勿将授权码告诉他人,以防系统被攻击!");

        message.setFrom("2718458328@qq.com");
        message.setTo("2718458328@qq.com");

        mailSender.send(message);

    }


    @RequestMapping("/enroll")
    public String toEnroll() {
        this.code();
        return "enroll";
    }

    @RequestMapping("/user/enroll")
    public String userenroll(@RequestParam String username, @RequestParam String password1, @RequestParam String password2, @RequestParam String usercode, Model model) {
        //具体业务
        if (this.usercode.equals(usercode)) {
            if (userService.getUserByName(username) == null && !StringUtils.isEmpty(username) && (password1).equals(password2)) {   //如果用户名未被注册且不为空且两次输入密码相同
                //将新用户写入数据库
                userService.addUser(new User(username, password1));
                model.addAttribute("msg2", "注册成功");
                return "index";                 //跳转登陆界面
            } else {
                model.addAttribute("msg3", "用户名为空或用户名已被注册或密码两次不一致!");
                return "enroll";                //返回注册界面
            }
        } else {
            model.addAttribute("msg3", "授权码不正确!");
            return "enroll";                //返回注册界面
        }

    }

    @RequestMapping("/login")
    public String login(@RequestParam String username, @RequestParam String password, @RequestParam String code, Model model, HttpSession session) {

        String Code = (String) session.getAttribute("code");

        user = userService.getUserByName(username);

        if (user != null && user.getUsername().equals(username) && user.getPassword().equals(password)) {
            if (code.equals(Code)) {
                //登录成功
                session.setAttribute("loginUser", username);
                return "redirect:/main.html";
            } else {
                //登录失败，提示用户
                model.addAttribute("msg", "验证码错误");
                return "index";
            }
        } else {
            //登录失败，提示用户
            model.addAttribute("msg", "用户名或密码错误");
            return "index";
        }
    }

    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "index";
    }


}
