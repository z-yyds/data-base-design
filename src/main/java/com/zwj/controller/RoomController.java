package com.zwj.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zwj.Service.RoomServiceImpl;
import com.zwj.Service.StateServiceImpl;
import com.zwj.Service.TypeServiceImpl;
import com.zwj.pojo.Person;
import com.zwj.pojo.Room;
import com.zwj.pojo.State;
import com.zwj.pojo.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/26 21:24
 */
@Controller
public class RoomController {

    @Autowired
    private RoomServiceImpl roomService;
    @Autowired
    private TypeServiceImpl typeService;
    @Autowired
    private StateServiceImpl stateService;

    @RequestMapping("/rooms")
    public String rooms(@RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "11") Integer pageSize, Model model) {

        PageHelper.startPage(pageNum, pageSize);

        try {
            List<Room> rooms = roomService.getRoomList();
            model.addAttribute("rooms", rooms);//查询出的数据传到前端
            PageInfo pageInfo = new PageInfo(rooms);//设置页面大小
            model.addAttribute("pageInfo", pageInfo);//将页面信息传到前端
        } finally {
            PageHelper.clearPage(); //清理 ThreadLocal 存储的分页参数,保证线程安全
        }

        return "room/list";
    }

    @RequestMapping("/room/query")
    public String roomQuery(@RequestParam String rid, @RequestParam int tid, @RequestParam int sid, Model model, @RequestParam int btn) {

        List<Room> rooms = null;
        //如果房间号为空
        if (rid.isEmpty()) {
            rid = null;
        }
        HashMap map = new HashMap();
        map.put("rid", rid);
        map.put("tid", tid);
        map.put("sid", sid);
        if (btn == 1) {
            //去精确查询
            rooms = roomService.queryRoom1(map);
        } else {
            //去模糊查询
            rooms = roomService.queryRoom2(map);
        }
        model.addAttribute("rooms", rooms);
        return "room/querylist";
    }

    @RequestMapping("/room/toAdd")
    public String toAddRoom(Model model) {
        List<Type> types = typeService.getTypeList();
        model.addAttribute("types", types);
        return "/room/add";
    }

    @RequestMapping("/room/add")
    public String addRoom(@RequestParam String rid, @RequestParam String tid, Model model) {

        Room flag = roomService.getRoomByRid(Integer.parseInt(rid));
        if (flag == null) {
            roomService.addRoom(new Room(Integer.valueOf(rid), Integer.valueOf(tid)));
            return "redirect:/rooms";
        } else {
            model.addAttribute("msg", "该房间号已存在，请重新添加!");
            return "/room/add";
        }
    }

    @RequestMapping("/room/toupdate/{id}")
    public String toUpdatePerson(@PathVariable("id") int id, Model model) {
        Room room = roomService.getRoomByRid(id);
        model.addAttribute("room", room);

        List<Type> types = typeService.getTypeList();
        model.addAttribute("types", types);

        List<State> states = stateService.getStateList();
        model.addAttribute("states", states);

        return "room/update";
    }

    @RequestMapping("/room/update")
    public String updatePerson(Room room) {
        roomService.updateRoom(room);
        return "redirect:/rooms";
    }

    @RequestMapping("/room/delete/{id}")
    public String deletePerson(@PathVariable("id") int id) {
        roomService.deleteRoom(id);
        return "redirect:/rooms";
    }


}
