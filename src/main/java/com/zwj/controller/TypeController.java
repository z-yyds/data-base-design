package com.zwj.controller;

import com.zwj.Service.TypeServiceImpl;
import com.zwj.pojo.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:21
 */
@Controller
public class TypeController {

    @Autowired
    private TypeServiceImpl typeService;

    @RequestMapping("/types")
    public String getTypeList(Model model) {
        List<Type> types = typeService.getTypeList();
        model.addAttribute("types", types);
        return "type/list";
    }

    @RequestMapping("/type/toAdd")
    public String toAdd() {
        return "/type/add";
    }

    @RequestMapping("/type/Add")
    public String typeAdd(@RequestParam String tname, @RequestParam String tprice) {

        typeService.addType(new Type(tname, Double.valueOf(tprice)));

        return "redirect:/types";
    }

    @RequestMapping("/type/delete/{id}")
    public String deleteType(@PathVariable("id") int id) {
        typeService.deleteType(id);
        return "redirect:/types";
    }

    @RequestMapping("/type/toupdate/{id}")
    public String toUpdate(@PathVariable("id") int id, Model model) {
        model.addAttribute("type", typeService.getTypeByTid(id));
        return "/type/update";
    }

    @RequestMapping("/type/update")
    public String update(Type type) {
        typeService.updateType(type);
        return "redirect:/types";
    }

}
