package com.zwj.controller;

import com.zwj.Service.OrderServiceImpl;
import com.zwj.Service.PersonService;
import com.zwj.Service.RoomServiceImpl;
import com.zwj.Service.TypeServiceImpl;
import com.zwj.pojo.Order;
import com.zwj.pojo.Person;
import com.zwj.pojo.Room;
import com.zwj.pojo.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/29 15:31
 */
@Controller
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;
    @Autowired
    private PersonService personService;
    @Autowired
    private RoomServiceImpl roomService;
    @Autowired
    private TypeServiceImpl typeService;

    @RequestMapping("/inorders")
    public String inOrders(Model model) {
        List<Order> orders = orderService.getIn();
        model.addAttribute("orders", orders);
        return "order/inlist";
    }

    @RequestMapping("/inorder/toadd")
    public String toAddOrder() {
        return "order/add";
    }

    @RequestMapping("/inorder/add")
    public String addInOrder(@RequestParam String pid, @RequestParam String rid, Model model) {

        Person person = personService.getPersonByPid(Integer.parseInt(pid));
        Room room = roomService.getRoomByRid(Integer.parseInt(rid));

        if (person == null) {
            model.addAttribute("msg", "顾客编号" + pid + "信息不存在，请先去录入顾客信息!");
            return "order/add";
        }

        if (room == null) {
            model.addAttribute("msg", "房间" + rid + "信息不存在，请仔细检查房间号!");
            return "order/add";
        }

        if (room.getSid() == 1) {
            model.addAttribute("msg", "房间" + rid + "已经有人入住了，请仔细检查房间号!");
            return "order/add";
        }

        Order order = new Order(Integer.parseInt(pid), Integer.parseInt(rid));
        //添加订单
        orderService.addOrder(order);

        return "redirect:/inorders";
    }

    @RequestMapping("/inorder/query")
    public String queryInOrders(@RequestParam String oid, @RequestParam String rid, @RequestParam String ptel, Model model) {

        if (oid.isEmpty()) {
            oid = null;
        }
        if (rid.isEmpty()) {
            rid = null;
        }
        if (ptel.isEmpty()) {
            ptel = null;
        }

        HashMap map = new HashMap();
        map.put("oid", oid);
        map.put("rid", rid);
        map.put("ptel", ptel);

        List<Order> orders = orderService.query(map);
        model.addAttribute("orders", orders);

        return "order/queryinlist";
    }

    @RequestMapping("/inorder/delete/{id}")
    public String deleteInorder(@PathVariable("id") int oid) {

        Order order = orderService.queryOrderByOid(oid);

        //删除订单
        orderService.deleteInOrder(oid);

        return "redirect:/inorders";
    }


    @RequestMapping("/outorders")
    public String outOrders(Model model) {
        List<Order> orders = orderService.getIn();
        model.addAttribute("orders", orders);
        return "order/outlist";
    }

    @RequestMapping("/outorder/query")
    public String queryOutOrders(@RequestParam String oid, @RequestParam String rid, @RequestParam String ptel, Model model) {

        if (oid.isEmpty()) {
            oid = null;
        }
        if (rid.isEmpty()) {
            rid = null;
        }
        if (ptel.isEmpty()) {
            ptel = null;
        }

        HashMap map = new HashMap();
        map.put("oid", oid);
        map.put("rid", rid);
        map.put("ptel", ptel);

        List<Order> orders = orderService.query(map);
        model.addAttribute("orders", orders);

        return "order/queryoutlist";
    }

    @RequestMapping("/outorder/delete/{id}")
    public String deleteoutorder(@PathVariable("id") int oid) {

        Order order = orderService.queryOrderByOid(oid);
        Room room = roomService.getRoomByRid(order.getRid());
        Type type = typeService.getTypeByTid(room.getTid());

        //修改退房时间
        order.setEnd(new Date());

        long mill = order.getEnd().getTime() - order.getStart().getTime();
        int day = (int) (mill / (24 * 60 * 60 * 1000));

        //计算所需费用
        order.setTotal(day * type.getTprice());
        //设置为已经付款
        order.setFlag(1);

        orderService.updateOrder(order);

        return "redirect:/outorders";
    }


    @RequestMapping("/finishorders")
    public String finishOrders(Model model) {
        List<Order> orders = orderService.getFinish();
        model.addAttribute("orders", orders);
        return "order/finishlist";
    }

    @RequestMapping("/finishorder/query")
    public String queryFinishOrders(@RequestParam String oid, @RequestParam String rid, @RequestParam String ptel, Model model) {

        if (oid.isEmpty()) {
            oid = null;
        }
        if (rid.isEmpty()) {
            rid = null;
        }
        if (ptel.isEmpty()) {
            ptel = null;
        }

        HashMap map = new HashMap();
        map.put("oid", oid);
        map.put("rid", rid);
        map.put("ptel", ptel);

        List<Order> orders = orderService.query2(map);
        model.addAttribute("orders", orders);

        return "order/queryfinishlist";
    }

    @RequestMapping("/finishorder/detail/{id}")
    public String orderDetail(@PathVariable("id") int oid, Model model) {

        Order order = orderService.queryOrderDetailByOid(oid);

        model.addAttribute("order", order);

        return "order/detail";
    }


}
