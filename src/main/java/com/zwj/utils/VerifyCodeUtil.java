package com.zwj.utils;


import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/22 22:07
 */
public class VerifyCodeUtil {

    //2023/2-13
    public static void test(){
        System.out.println("本方法是我测试git合并分支时候使用，不具备任何功能");
    }


    public static void testMaster(){
        System.out.println("本方法是我测试git合并Master分支时候使用，不具备任何功能");
    }


    public static String getCode() {

        StringBuilder builder = new StringBuilder();

        for (char i = 'A'; i <= 'Z'; i++) {
            builder.append(i);
        }
        for (char i = 'a'; i <= 'z'; i++) {
            builder.append(i);
        }
        for (char i = '0'; i <= '9'; i++) {
            builder.append(i);
        }

        StringBuilder builder1 = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < 6; i++) {

            int index = r.nextInt(builder.length());
            char c = builder.charAt(index);
            builder1.append(c);
        }

        int i = r.nextInt(57 - 48 + 1) + 48;
        char c = (char) i;
        int index = r.nextInt(builder1.length());
        builder1.setCharAt(index, c);

        return builder1.toString();
    }

}
