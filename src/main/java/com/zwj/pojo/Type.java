package com.zwj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/27 9:31
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Type {

    private int tid;
    private String tname;
    private double tprice;

    public Type(String tname, double tprice) {
        this.tname = tname;
        this.tprice = tprice;
    }
}
