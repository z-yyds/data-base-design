package com.zwj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/28 16:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Order {

    private int oid;    //订单号

    private int pid;    //入住人编号
    private String pname;
    private int psex;
    private String ptel;

    private int rid;    //入住房间号
    private int tid;
    private String tname;
    private double tprice;

    private int sid;
    private String sname;

    private int flag;

    private Date start;
    private Date end;
    private double total;


    public Order(int pid, int rid) {
        this.pid = pid;
        this.rid = rid;
        this.start = new Date();
        this.end = new Date();
        total = 0.0;
        flag = 0;
    }


}
