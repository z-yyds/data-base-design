package com.zwj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/27 20:50
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private int pid;    //客人编号
    private String pname;
    private int psex;   //性别 0女1男
    private String sex;
    private String ppid;    //身份证号
    private String ptel;

    public Person(String pname, int psex, String ppid, String ptel) {
        this.pname = pname;
        this.psex = psex;
        this.ppid = ppid;
        this.ptel = ptel;
    }
}
