package com.zwj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/26 21:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//客房实体类
public class Room {

    private int rid; //序号id，主键
    private int tid;  //类型id,外键
    private String tname;
    private int sid;  //状态id,外键
    private String sname;
    private double price;    //价格

    public Room(int rid, int tid) {
        this.rid = rid;
        this.tid = tid;
        this.sid = 2;
    }
}
