package com.zwj.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 21:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class State {

    private int sid;
    private String sname;

}
