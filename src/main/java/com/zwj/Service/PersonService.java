package com.zwj.Service;

import com.zwj.pojo.Person;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:06
 */
public interface PersonService {

    List<Person> getPersonList();

    List<Person> queryPerson(Map map);

    int addPerson(Person person);

    Person getPersonByPid(int pid);

    Person getPersonByPpid(String ppid);

    int updatePerson(Person person);

    int deletePerson(int pid);

}
