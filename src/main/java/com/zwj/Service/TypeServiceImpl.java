package com.zwj.Service;

import com.zwj.mapper.TypeMapper;
import com.zwj.pojo.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:03
 */
@Service
@Transactional
public class TypeServiceImpl implements TypeService {

    @Autowired
    private TypeMapper typeMapper;

    @Override
    public List<Type> getTypeList() {
        return typeMapper.getTypeList();
    }

    @Override
    public Type getTypeByTid(int tid) {
        return typeMapper.getTypeByTid(tid);
    }

    @Override
    public int addType(Type type) {
        return typeMapper.addType(type);
    }

    @Override
    public int deleteType(int tid) {
        return typeMapper.deleteType(tid);
    }

    @Override
    public int updateType(Type type) {
        return typeMapper.updateType(type);
    }
}
