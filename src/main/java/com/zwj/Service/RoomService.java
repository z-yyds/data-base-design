package com.zwj.Service;

import com.zwj.pojo.Room;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 15:51
 */
public interface RoomService {

    List<Room> getRoomList();

    List<Room> queryRoom1(Map map);

    List<Room> queryRoom2(Map map);

    Room getRoomByRid(int rid);

    int addRoom(Room room);

    int updateRoom(Room room);

    int deleteRoom(int rid);

}
