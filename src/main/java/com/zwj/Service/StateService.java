package com.zwj.Service;

import com.zwj.pojo.State;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 21:34
 */
public interface StateService {

    List<State> getStateList();

    State getStateBySid(int sid);

    int addState(String sname);

    int updateState(State state);

    int deleteState(int sid);

}
