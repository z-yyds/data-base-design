package com.zwj.Service;

import com.zwj.pojo.Order;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 15:56
 */
public interface OrderService {

    int addOrder(Order order);

    List<Order> getIn();

    List<Order> getFinish();

    List<Order> query(Map map);

    List<Order> query2(Map map);

    Order queryOrderByOid(int oid);

    Order queryOrderDetailByOid(int oid);

    int deleteInOrder(int oid);

    int updateOrder(Order order);

}
