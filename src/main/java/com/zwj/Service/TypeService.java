package com.zwj.Service;

import com.zwj.pojo.Type;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:02
 */
public interface TypeService {

    List<Type> getTypeList();

    Type getTypeByTid(int tid);

    int addType(Type type);

    int deleteType(int tid);

    int updateType(Type type);

}
