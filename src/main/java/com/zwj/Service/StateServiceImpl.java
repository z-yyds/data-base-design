package com.zwj.Service;

import com.zwj.mapper.StateMapper;
import com.zwj.pojo.State;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 21:35
 */
@Service
@Transactional
public class StateServiceImpl implements StateService {

    @Autowired
    private StateMapper stateMapper;

    @Override
    public List<State> getStateList() {
        return stateMapper.getStateList();
    }

    @Override
    public State getStateBySid(int sid) {
        return stateMapper.getStateBySid(sid);
    }

    @Override
    public int addState(String sname) {
        return stateMapper.addState(sname);
    }

    @Override
    public int updateState(State state) {
        return stateMapper.updateState(state);
    }

    @Override
    public int deleteState(int sid) {
        return stateMapper.deleteState(sid);
    }
}
