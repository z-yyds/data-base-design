package com.zwj.Service;

import com.zwj.mapper.PersonMapper;
import com.zwj.pojo.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:06
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonMapper personMapper;

    @Override
    public List<Person> getPersonList() {
        return personMapper.getPersonList();
    }

    @Override
    public List<Person> queryPerson(Map map) {
        return personMapper.queryPerson(map);
    }

    @Override
    public int addPerson(Person person) {
        return personMapper.addPerson(person);
    }

    @Override
    public Person getPersonByPid(int pid) {
        return personMapper.getPersonByPid(pid);
    }

    @Override
    public Person getPersonByPpid(String ppid) {
        return personMapper.getPersonByPpid(ppid);
    }

    @Override
    public int updatePerson(Person person) {
        return personMapper.updatePerson(person);
    }

    @Override
    public int deletePerson(int pid) {
        return personMapper.deletePerson(pid);
    }
}
