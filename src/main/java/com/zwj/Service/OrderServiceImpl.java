package com.zwj.Service;

import com.zwj.mapper.OrderMapper;
import com.zwj.pojo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 15:57
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderMapper orderMapper;

    @Override
    public int addOrder(Order order) {
        return orderMapper.addOrder(order);
    }

    @Override
    public List<Order> getIn() {
        return orderMapper.getIn();
    }

    @Override
    public List<Order> getFinish() {
        return orderMapper.getFinish();
    }

    @Override
    public List<Order> query(Map map) {
        return orderMapper.query(map);
    }

    @Override
    public List<Order> query2(Map map) {
        return orderMapper.query2(map);
    }

    @Override
    public Order queryOrderByOid(int oid) {
        return orderMapper.queryOrderByOid(oid);
    }

    @Override
    public Order queryOrderDetailByOid(int oid) {
        return orderMapper.queryOrderDetailByOid(oid);
    }

    @Override
    public int deleteInOrder(int oid) {
        return orderMapper.deleteInOrder(oid);
    }

    @Override
    public int updateOrder(Order order) {
        return orderMapper.updateOrder(order);
    }
}
