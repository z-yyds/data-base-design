package com.zwj.Service;

import com.zwj.mapper.RoomMapper;
import com.zwj.pojo.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 15:51
 */
@Service
@Transactional
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomMapper roomMapper;

    @Override
    public List<Room> getRoomList() {
        return roomMapper.getRoomList();
    }

    @Override
    public List<Room> queryRoom1(Map map) {
        return roomMapper.queryRoom1(map);
    }

    @Override
    public List<Room> queryRoom2(Map map) {
        return roomMapper.queryRoom2(map);
    }

    @Override
    public Room getRoomByRid(int rid) {
        return roomMapper.getRoomByRid(rid);
    }

    @Override
    public int addRoom(Room room) {
        return roomMapper.addRoom(room);
    }

    @Override
    public int updateRoom(Room room) {
        return roomMapper.updateRoom(room);
    }

    @Override
    public int deleteRoom(int rid) {
        return roomMapper.deleteRoom(rid);
    }

}
