package com.zwj.Service;

import com.zwj.pojo.User;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 16:13
 */
public interface UserService {

    User getUserByName(String username);

    int addUser(User user);

}
