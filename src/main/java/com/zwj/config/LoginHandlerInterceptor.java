package com.zwj.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/29 20:38
 */

public class LoginHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse resp, Object handler) throws Exception {

        //登陆成功，应该有用户session
        Object loginUser = req.getSession().getAttribute("loginUser");

        if (loginUser == null) {
            req.setAttribute("msg", "没有权限，请先登录");
            req.getRequestDispatcher("/index.html").forward(req, resp);
            return false;
        } else {
            return true;
        }

    }

}
