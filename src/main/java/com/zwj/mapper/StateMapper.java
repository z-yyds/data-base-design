package com.zwj.mapper;

import com.zwj.pojo.State;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/7/18 21:32
 */
@Repository
public interface StateMapper {

    List<State> getStateList();

    State getStateBySid(int sid);

    int addState(String sname);

    int updateState(State state);

    int deleteState(int sid);

}
