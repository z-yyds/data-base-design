package com.zwj.mapper;

import com.zwj.pojo.Type;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/27 10:22
 */
@Repository
public interface TypeMapper {

    List<Type> getTypeList();

    Type getTypeByTid(int tid);

    int deleteType(int tid);

    int updateType(Type type);

    int addType(Type type);

}
