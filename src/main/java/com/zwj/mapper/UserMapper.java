package com.zwj.mapper;

import com.zwj.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/23 12:22
 */
@Repository
public interface UserMapper {

    User getUserByName(String username);

    int addUser(User user);

}
