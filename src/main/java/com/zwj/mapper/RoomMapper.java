package com.zwj.mapper;

import com.zwj.pojo.Room;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/26 21:26
 */
@Repository
public interface RoomMapper {

    List<Room> getRoomList();

    List<Room> queryRoom1(Map map);

    List<Room> queryRoom2(Map map);

    Room getRoomByRid(int rid);

    int addRoom(Room room);

    int updateRoom(Room room);

    int deleteRoom(int rid);

}
