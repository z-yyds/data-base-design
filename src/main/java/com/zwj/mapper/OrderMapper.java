package com.zwj.mapper;

import com.zwj.pojo.Order;
import com.zwj.pojo.Room;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 笨手笨脚の
 * @create 2022/6/26 21:26
 */
@Repository
public interface OrderMapper {

    int addOrder(Order order);

    List<Order> getIn();

    List<Order> getFinish();

    List<Order> query(Map map);

    List<Order> query2(Map map);

    Order queryOrderByOid(int oid);

    Order queryOrderDetailByOid(int oid);

    int deleteInOrder(int oid);

    int updateOrder(Order order);


}
