package com.zwj.mapper;

import com.zwj.pojo.Person;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @Author : 朱文杰
 * @create 2022/6/27 20:59
 */
@Repository
public interface PersonMapper {

    List<Person> getPersonList();

    List<Person> queryPerson(Map map);

    int addPerson(Person person);

    Person getPersonByPid(int pid);

    Person getPersonByPpid(String ppid);

    int updatePerson(Person person);

    int deletePerson(int pid);

}
