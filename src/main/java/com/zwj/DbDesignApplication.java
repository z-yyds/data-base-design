package com.zwj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("com.zwj.mapper")
public class DbDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbDesignApplication.class, args);
    }

}
